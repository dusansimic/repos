# Dušan's repos

My repos for Linux packages. Here I put some useful packages and programs that don't exist in
official repositories.

## DEB packages

### Recommended method

```bash
wget -qO - https://repos.dusansimic.me/repo.gpg | sudo apt-key --keyring /etc/apt/trusted.gpg.d/dusan.gpg add -
sudo apt-add-repository 'deb https://repos.dusansimic.me/debs dusan main'
```

### Alterantive method

```bash
wget -qO - https://repos.dusansimic.me/key.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/dusan.gpg
echo "deb [ signed-by=/usr/share/keyrings/dusan.gpg ] https://repos.dusansimic.me/debs dusan main" | sudo tee /etc/apt/sources.list.d/dusan.list
```

### Update package list

```bash
sudo apt update
```

## RPM packages

### Add the repo

```bash
sudo tee -a /etc/yum.repos.d/dusan.repo << 'EOF'
[dusan]
name=dusan
baseurl=https://repos.dusansimic.me/rpms
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://repos.dusansimic.me/key.gpg
metadata_expire=1h
EOF
```

You're ready to install packages!


### About

This repo is being built using [Tiny Repo](https://gitlab.com/dusansimic/tinyrepo) script.
